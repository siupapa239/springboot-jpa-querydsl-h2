package kr.sskm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJpaQuerydslH2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJpaQuerydslH2Application.class, args);
	}

}
