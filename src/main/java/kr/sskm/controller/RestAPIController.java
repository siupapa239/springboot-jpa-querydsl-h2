package kr.sskm.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.sskm.domain.Member;
import kr.sskm.domain.MemberAddress;
import kr.sskm.dto.ApiResponse;
import kr.sskm.dto.SearchDto;
import kr.sskm.repository.MemberAddressRepository;
import kr.sskm.repository.MemberRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value="v1")
public class RestAPIController {
	
	@Autowired
	private MemberRepository mrepo;
	
	@Autowired
	private MemberAddressRepository arepo;
	
	
	@GetMapping(value= "/test1", produces = { "application/json; charset=utf-8" })
	public ResponseEntity<Object> test1(HttpServletRequest request, HttpServletResponse response, Model model){
		
		ApiResponse<Object> apiResponse = null;
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		try {
			
	        Member member = new Member();
			member.setId("TEST3");
			member.setPassword("3333");
			Member saveMember = mrepo.save(member);
			log.info("member_idx =====> " + saveMember.getMember_idx());
			
			MemberAddress address = new MemberAddress();
			address.setAddr1("AAA");
			address.setAddr2("BBB");
			address.setMember(saveMember);
			MemberAddress saveAddress = arepo.save(address);
			log.info("Member_address_idx =====> " + saveAddress.getMember_address_idx());
			log.info(saveAddress.toString());
	        
			apiResponse = new ApiResponse<>(HttpStatus.OK, "success", resultMap);
		}catch(Exception e) {
			
			e.printStackTrace();
			
			resultMap.put("message", e.getMessage());
			resultMap.put("rdata", "N");
	    	apiResponse = new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, "error", resultMap);
	    	return new ResponseEntity<Object>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	    	
		}
		
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
}
