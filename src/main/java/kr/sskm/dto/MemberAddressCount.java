package kr.sskm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberAddressCount {
	private Long member_idx;
	private String id;
	private Long count;
}
