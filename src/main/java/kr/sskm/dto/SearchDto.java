package kr.sskm.dto;


import lombok.Data;

@Data
public class SearchDto {
	private String searchWord;
	private String id;
}

