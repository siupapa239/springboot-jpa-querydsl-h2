package kr.sskm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.sskm.domain.Member;

public interface MemberRepository extends JpaRepository<Member, Long>, MemberRepositoryCustom{
	
}
