package kr.sskm.repository.impl;

import static kr.sskm.domain.QMember.member;
import static kr.sskm.domain.QMemberAddress.memberAddress;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

import kr.sskm.domain.Member;
import kr.sskm.dto.MemberAddressCount;
import kr.sskm.dto.SearchDto;
import kr.sskm.repository.MemberRepositoryCustom;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MemberRepositoryImpl extends QuerydslRepositorySupport implements MemberRepositoryCustom{
	
	public MemberRepositoryImpl() {
		super(Member.class);
	}
	
	@Override
	public List<Member> findByJoinList(SearchDto dto, @PageableDefault Pageable pageable){
		
		JPQLQuery<Member> query = from(member);
		query.leftJoin(memberAddress)
				.on(member.member_idx.eq(memberAddress.member.member_idx))
				.orderBy(member.reg_dt.desc());
		
		List<Member> list = getQuerydsl().applyPagination(pageable, query).fetch();
		
		
		return list; 
		
	}
	
	@Override
	public void simpleSubQuery(){
		
        JPQLQuery<Member> query = from(member);
        query.select(Projections.fields(MemberAddressCount.class, 
        		member.member_idx,
        		member.id,
                ExpressionUtils.as(
                        JPAExpressions.select(memberAddress.member.count()).from(memberAddress).where(memberAddress.member.eq(member)), "memberAddressCount")
        ))
        .fetch()
        .stream()
        .forEach(result -> {
            log.info("team name is : " + result.getId());
            log.info("member count is : " + result.getCount());
        });
        
	}
	
	@Override
	public void selectBooleanExpressionList(SearchDto dto, @PageableDefault Pageable pageable){
		
		JPQLQuery<Member> query = from(member);
		query.where (
				equalId(dto.getId()),
				likeSearchWord(dto.getSearchWord())
		)
		.fetch()
		.stream()
		.forEach(result -> {
			log.info("result.id : " + result.getId());
			log.info("result.password : " + result.getPassword());
		});
		
	}
	
	@Override
	public void selectBooleanBuilderList(SearchDto dto, @PageableDefault Pageable pageable){
		
		String id = dto.getId();
		String searchWord = dto.getSearchWord();

		BooleanBuilder builder = new BooleanBuilder();
        if (!StringUtils.isEmpty(id)) {
            builder.and(member.id.eq(id));
        }
        if (!StringUtils.isEmpty(searchWord)) {
            builder.or(member.id.eq(searchWord));
            builder.or(member.password.eq(searchWord));
        }
		
		JPQLQuery<Member> query = from(member);
		query.where (
				equalId(dto.getId()),
				likeSearchWord(dto.getSearchWord())
		)
		.fetch()
		.stream()
		.forEach(result -> {
			log.info("result.id : " + result.getId());
			log.info("result.password : " + result.getPassword());
		});
	}
	
	private BooleanExpression equalId(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}
		return member.id.stringValue().eq(id);
	}
	
	private BooleanExpression likeSearchWord(String searchWord) {
        if (StringUtils.isEmpty(searchWord)) {
            return null;
        }
        return member.id.contains(searchWord).or(member.password.contains(searchWord)); // OR
    }
}
