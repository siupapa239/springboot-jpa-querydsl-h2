package kr.sskm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.sskm.domain.MemberAddress;

public interface MemberAddressRepository extends JpaRepository<MemberAddress, Long>{
	
}
