package kr.sskm.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import kr.sskm.domain.Member;
import kr.sskm.dto.SearchDto;

public interface MemberRepositoryCustom {
	List<Member> findByJoinList(SearchDto dto, @PageableDefault Pageable pageable);
	void simpleSubQuery();
	void selectBooleanBuilderList(SearchDto dto, @PageableDefault Pageable pageable);
	void selectBooleanExpressionList(SearchDto dto, @PageableDefault Pageable pageable);
}
