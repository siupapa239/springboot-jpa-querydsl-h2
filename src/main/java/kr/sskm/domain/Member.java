package kr.sskm.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "tb_member")
public class Member implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4391936848612174224L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long member_idx;
	private String id;
	private String password;
	@CreationTimestamp
	private LocalDateTime reg_dt;
	
	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval=true)
	List<MemberAddress> list = new ArrayList<MemberAddress>();

	@Override
	public String toString() {
		return "Member [member_idx=" + member_idx + ", id=" + id + ", password=" + password + ", reg_dt=" + reg_dt
				+  "]";
	}
	
}
