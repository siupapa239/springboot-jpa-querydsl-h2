package kr.sskm.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import kr.sskm.domain.customenum.YNEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "tb_member_address")
public class MemberAddress implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3081150473785163652L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long member_address_idx;
	private String addr1;
	private String addr2;
	@Enumerated(EnumType.STRING)
	private YNEnum basic_yn;
	
	@ManyToOne(targetEntity = Member.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "member_idx")
	private Member member;

	@Override
	public String toString() {
		return "MemberAddress [member_address_idx=" + member_address_idx + ", addr1=" + addr1 + ", addr2=" + addr2
				+ ", basic_yn=" + basic_yn + ", member=" + member + "]";
	}
	
}
