package kr.sskm.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMember is a Querydsl query type for Member
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMember extends EntityPathBase<Member> {

    private static final long serialVersionUID = -59422241L;

    public static final QMember member = new QMember("member1");

    public final StringPath id = createString("id");

    public final ListPath<MemberAddress, QMemberAddress> list = this.<MemberAddress, QMemberAddress>createList("list", MemberAddress.class, QMemberAddress.class, PathInits.DIRECT2);

    public final NumberPath<Long> member_idx = createNumber("member_idx", Long.class);

    public final StringPath password = createString("password");

    public final DateTimePath<java.time.LocalDateTime> reg_dt = createDateTime("reg_dt", java.time.LocalDateTime.class);

    public QMember(String variable) {
        super(Member.class, forVariable(variable));
    }

    public QMember(Path<? extends Member> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMember(PathMetadata metadata) {
        super(Member.class, metadata);
    }

}

