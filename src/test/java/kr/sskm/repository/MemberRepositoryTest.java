package kr.sskm.repository;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import kr.sskm.domain.Member;
import kr.sskm.domain.MemberAddress;
import kr.sskm.dto.SearchDto;

@SpringBootTest
class MemberRepositoryTest {
	
	@Autowired
	MemberRepository repository;
	
	@Autowired
	MemberAddressRepository addressRepository;

	@Test
	public void member_memberAddress_목록조회() {
		
		SearchDto dto = new SearchDto();
		Pageable pageable = PageRequest.of(0, 3);
		List<Member> list = repository.findByJoinList(dto, pageable);
		
		System.out.println("=======================================================================================");
		System.out.println(list.size());
		System.out.println("=======================================================================================");
		
	}
	
	@Test
	public void simpleSubQuery_서브쿼리목록조회() {
		repository.simpleSubQuery();
	}
	
	@Test
	public void selectBooleanBuilderList_목록조회() {
		SearchDto dto = new SearchDto();
		dto.setId("TEST1");
		dto.setSearchWord("1111");
		Pageable pageable = PageRequest.of(0, 3);
		repository.selectBooleanBuilderList(dto, pageable);
	}
	
	@Test
	public void selectBooleanExpressionList_목록조회() {
		SearchDto dto = new SearchDto();
		dto.setId("TEST2");
		dto.setSearchWord("1111");
		Pageable pageable = PageRequest.of(0, 3);
		repository.selectBooleanExpressionList(dto, pageable);
	}
	
	@Test
	public void member저장_member_address저장() {
		
		Member member = new Member();
		member.setId("TEST3");
		member.setPassword("3333");
		Member saveMember = repository.save(member);
		System.out.println("member_idx =====> " + saveMember.getMember_idx());
		
//		MemberAddress address = new MemberAddress();
//		address.setAddr1("AAA");
//		address.setAddr2("BBB");
//		address.setMember(saveMember);
//		MemberAddress saveAddress = addressRepository.save(address);
//		System.out.println("Member_address_idx =====> " + saveAddress.getMember_address_idx());
		
		
	}
	
	// fetch join!!
	// 3~4개 테이블 조인!!
}

