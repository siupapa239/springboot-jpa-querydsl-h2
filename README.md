## =========== 개발환경 ===========
  
Springboot + Gradle5.x + Spring Data JPA + Querydsl + SpringbootTest(junit5) + h2 로컬 개발환경  
  
## =========== build.gradle 설정 ===========  
springboot-jpa-querydsl dependency 설정에 h2설정만 추가     
compile 'com.h2database:h2'			 // h2 (runtime=>compile로 변경)  

## =========== 설명 ===========  
### spring.jpa.hiberante.ddl-auto 속성   
none: 자동 생성하지 않음  
create: 항상 다시 생성  
create-drop: 시작 시 생성 후 종료 시 제거  
update: 시작 시 Entity 클래스와 DB 스키마 구조를 비교해서 DB쪽에 생성되지 않은 테이블, 컬럼 추가 (제거는 하지 않음)  
validate: 시작 시 Entity 클래스와 DB 스키마 구조를 비교해서 같은지만 확인 (다르면 예외 발생)  
  
### Test case 
MemberRepositoryTest > member_memberAddress_목록조회  - left join, 페이징  
MemberRepositoryTest > simpleSubQuery_서브쿼리목록조회 - Projections.fields를 이용한 subuery, ExpressionUtils.as 서브쿼리 컬럼 alias지정  
MemberRepositoryTest > selectBooleanBuilderList_목록조회   
MemberRepositoryTest > selectBooleanExpressionList_목록조회  
    
## =========== 참고 ===========
https://dico.me/java/articles/241 (springboot-h2설정)  
https://velog.io/@owljoa/%EC%88%98%EC%A0%95%ED%95%84%EC%9A%94-JPA-Hibernate-%EC%B4%88%EA%B8%B0-%EB%8D%B0%EC%9D%B4%ED%84%B0-%EC%83%9D%EC%84%B1- (JPA-Hibernate 속성)  


